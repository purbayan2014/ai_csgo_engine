import requests
import streamlit as st
import pandas as pd 
import numpy as np
import pickle
import matplotlib.pyplot as plt
import plotly
from plotly.offline import plot, iplot, download_plotlyjs, init_notebook_mode
import plotly.graph_objects as go
import plotly.figure_factory as ff
from PIL import Image

st.title('CSGO AI Engine Prediction')

def main():
    page = st.sidebar.selectbox(
        "Select a Page",
        [
            "Homepage","Visual Data Exploration","Models Used","About"
        ]
    )

    if page == "Homepage":
        homepage()
    elif page == "Visual Data Exploration":
        VDA()
    elif page == "Models Used":
        Model()
    elif page == "About":
        About()

def About():
    profile = Image.open('profile.jpeg')
    st.image(profile,caption=" ")
    st.markdown("Hello there I am Purbayan Majumder a pre-final year IT student. I am Highly proficient in Machine Learning,Reverse Enginerring,Exploitation Development,Cyber security,Deep Learning ,Neural networks ,RNN , CNN and NLP.")
    st.markdown(" ")
    st.markdown("Wanna Contact me for some help on any project related to AI or cyber-security then just sent me a mail at purbayan2014@gmail.com")
def Model():
    st.header('Models Used to implement this Engine')
    # RFCC
    imagerfc = Image.open('rfcc.png')
    st.image(imagerfc, caption='Random Forest Classifier')
    st.markdown("Random forest is a commonly-used machine learning algorithm trademarked by Leo Breiman and Adele Cutler,which combines the output of multiple decision trees to reach a single result.")
    st.markdown("")
    st.markdown("Its ease of use and flexibility have fueled its adoption, as it handles both classification and regression problems.")

    # MLP
    imagemlp = Image.open('mlp.png')
    st.image(imagemlp, caption='Multi-Layer Perceptron Classifier')
    st.markdown("MLPClassifier trains iteratively since at each time step the partial derivatives of the loss function with respect to the model parameters are computed to update the parameters.It can also have a regularization term added to the loss function that shrinks model parameters to prevent overfitting.This implementation works with data represented as dense numpy arrays or sparse scipy arrays of floating point values.")

    #XGBOOST AND LIGHTGBM
    imageboost = Image.open('XGLIGHT.png')
    st.image(imageboost, caption='Xgboost and Light Gradient Boosting ')
    st.markdown("XGBoost, which stands for Extreme Gradient Boosting, is a scalable, distributed gradient-boosted decision tree (GBDT) machine learning library. It provides parallel tree boosting and is the leading machine learning library for regression, classification, and ranking problems.It’s vital to an understanding of XGBoost to first grasp the machine learning concepts and algorithms that XGBoost builds upon: supervised machine learning, decision trees, ensemble learning, and gradient boosting.")
    st.markdown("           ")
    st.markdown("In machine learning, the LightGBM classifier is part of the Boosting family, and today it is the most common classification model in the machine learning community. LightGBM is a powerful machine learning model that can be shaped depending on the task you are working on.")

    # Decision Tree
    imagetree = Image.open('desctree.png')
    st.image(imagetree, caption='Decision Tree Classifier')
    st.markdown("A decision tree is a non-parametric supervised learning algorithm, which is utilized for both classification and regression tasks. It has a hierarchical, tree structure, which consists of a root node, branches, internal nodes and leaf nodes.")

    # CatBoost
    st.markdown("CatBoost or Categorical Boosting is an open-source boosting library developed by Yandex. In addition to regression and classification, CatBoost can be used in ranking, recommendation systems, forecasting and even personal assistants. ")

    # Gradient Boosting
    st.markdown("Gradient Boosting is an ensemble machine learning algorithm and typically used for solving classification and regression problems. It is easy to use and works well with heterogeneous data and even relatively small data. It essentially creates a strong learner from an ensemble of many weak learners.")

def VDA():
    df = pd.read_csv('csgo_round_snapshots.csv')
    used_df = df.drop(df.iloc[:, 16:96], axis = 1)

    uMap = set(used_df['map'])
    CTwins = []
    Twins = []
    MAP = []

    for mem in uMap:
        MAP.append(mem)
        CTwins.append( len(used_df[ (used_df['map'] == mem) & (used_df['round_winner'] == 'CT')]) )
        Twins.append( len(used_df[ (used_df['map'] == mem) & (used_df['round_winner'] == 'T')]) )

    st.header("Bar Graph : Map Wise Win Distribution Between CT and T sides ")

    fig = plt.figure(figsize = (16 ,7))
    plt.subplot(1,2,1)
    plt.rcParams.update({'font.size': 22})
    plt.bar(MAP, CTwins, label = ' CT wins')
    plt.xticks(rotation = 'vertical')
    plt.title('CT wins Mapwise Distribution')
    plt.grid(True)
    plt.legend()

    plt.subplot(1,2,2)
    plt.rcParams.update({'font.size': 22})
    plt.bar(MAP, Twins, label = ' T wins', color = 'orange')
    plt.xticks(rotation = 'vertical')
    plt.title('T wins Mapwise Distribution')
    plt.grid(True)
    plt.legend()
    st.pyplot(fig)

    labels = ['T wins', 'CT wins']
    values = [len(used_df[used_df['round_winner'] == 'T']) , len(used_df[used_df['round_winner'] == 'CT'])]

    

    fig_plotly1 = go.Figure(go.Pie(
    labels = labels,
    values = values,
    ))

    fig_plotly1.update_layout(
        font=dict(
        family="Courier New, monospace",
        size=15,
        color="RebeccaPurple")
    ) 
    st.header('PieChart: T Vs CT wins')
    st.plotly_chart(fig_plotly1)

    x_val = ["T wins", "CT wins"]
    y_label1 = [
    len(used_df[(used_df['round_winner'] == 'T') & (used_df['bomb_planted']== True)]),
    len(used_df[(used_df['round_winner'] == 'CT') & (used_df['bomb_planted']== True)])
           ]

    y_label2 = [
    len(used_df[(used_df['round_winner'] == 'T') & (used_df['bomb_planted']== False)]),
    len(used_df[(used_df['round_winner'] == 'CT') & (used_df['bomb_planted']== False)])
    ]

    ## We here are plotting two different series of bars
    ## 1.) T Vs CT wins when bomb was Planted
    ## 2.) T Vs CT wins when bomb was not Planted

    trace1 = go.Bar(
    x = x_val,
    y = y_label1,
    name='T Vs CT Wins when bomb was planted'
    )

    trace2 =  go.Bar(
    x = x_val,
    y = y_label2,
    name='T Vs CT Wins when bomb was Not  planted'
    )

    data  = [trace1, trace2]
    layout = go.Layout(barmode='group')

    fig = go.Figure(data = data, layout= layout)
    st.header('Histogram : T vs CT Win Distribution when bomb was planted or not planted ')
    st.plotly_chart(fig)

    #        x = health                                      #
#        y = armor                                       #
#        z =  money                                      #
##########################################################

    trace11 = go.Scatter3d(
        x = used_df[used_df['round_winner'] == 'CT'].head(500).ct_health,
        y = used_df[used_df['round_winner'] == 'CT'].head(500).ct_armor,
        z = used_df[used_df['round_winner'] == 'CT'].head(500).ct_money,
        mode = 'markers',
        name= 'T wins'
    )

    trace22 = go.Scatter3d(
        x = used_df[used_df['round_winner'] == 'T'].head(500).t_health,
        y = used_df[used_df['round_winner'] == 'T'].head(500).t_armor,
        z = used_df[used_df['round_winner'] == 'T'].head(500).t_money,
        mode = 'markers',
        name = 'CT wins'
    )

    data_scatter3d = [trace11,trace22]

    fig = go.Figure(data = data_scatter3d)
    st.header('3D Scatter-Plot : T vs CT based on health,armor and money ')
    st.plotly_chart(fig)


    fig = ff.create_distplot([used_df[used_df['round_winner'] == 'T'].t_health,
                          used_df[used_df['round_winner'] == 'CT'].ct_health],
                         ['health distribution Plot when T wins', 'health distribution when CT wins'])
    st.header('Health Distribution when CT and T wins ')
    st.plotly_chart(fig)






def homepage():

    main_model_engine = pickle.load(open('csgo_rfc_model_not_scaled.pkl', 'rb'))

    time_left =  st.number_input('How much time is left in the match ? ')

    ct_score =  st.number_input('What is the current CT side score ? ')

    t_score =  st.number_input('What is the current T side score ? ')

    bomb_planted = st.number_input('Is the Bomb Planted (1/0) ?')

    ct_health = st.number_input('What is the CT health ? ')

    t_health = st.number_input('What is the t health ?')

    ct_armor = st.number_input('How much CT armor is left ? ')

    t_armor = st.number_input('How much T armor is left approx do u guess ? ')

    ct_money = st.number_input('How much CT money is left ? ')

    t_money = st.number_input('How much T money is left ? ')

    ct_helmets = st.number_input('How many CT has a helmet ? ')

    t_helmets = st.number_input('How many T has a helmet ? ')

    ct_defuse_kits = st.number_input('How many CT has a defuse Kit ? ')

    ct_players_alive = st.number_input('How many CT players are alive ? ')

    t_players_alive = st.number_input('How many T players are alive ? ')

    de_cache = st.number_input('Are you playing on cache ? (0/1) ')

    de_dust2 = st.number_input('Are you playing on dust2 ? (0/1) ')

    de_inferno = st.number_input('Are you playing on inferno ? (0/1) ')

    de_mirage = st.number_input('Are you playing on mirage ? (0/1) ')

    de_nuke = st.number_input('Are you playing on nuke ? (0/1)')

    de_overpass = st.number_input('Are you playing on overpass ? (0/1)')

    de_train = st.number_input('Are you playing on train ? (0/1)')

    de_vertigo = st.number_input('Are you playing on vertigo ? (0/1)')

    test_dt = np.array([[time_left,ct_score,t_score,bomb_planted,ct_health,t_health,ct_armor,t_armor,
    ct_money,t_money,ct_helmets,t_helmets,ct_defuse_kits,ct_players_alive,t_players_alive,
    de_cache,de_dust2,de_inferno,de_mirage,de_nuke,de_overpass,de_train,de_vertigo]],dtype=object)



    if st.button('Predict'):
        print(test_dt.shape)
        res = main_model_engine.predict(test_dt)
        
        if (res == 0):
            st.markdown("Welp Better luck next time you will lose this match !!!!")
            
        elif (res == 1):
            st.markdown("As per my prediction I think you will win today ")


if __name__ == "__main__":
    main()